export class MongoClient {
  public connect: any;
  public db: any;
  public collection: any;
  public insertOne: any;
  public find: any;
  public sort: any;
  public limit: any;
  public toArray: any;

  constructor() {
    this.connect = jest.fn().mockResolvedValue("");
    this.db = jest.fn(() => this);
    this.collection = jest.fn(() => this);
    this.insertOne = jest.fn();
    this.find = jest.fn(() => this);
    this.sort = jest.fn(() => this);
    this.limit = jest.fn(() => Promise.resolve(this));
    this.toArray = jest.fn();
  }
}
