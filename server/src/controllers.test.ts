import { executeCmd } from "./executor";
import { saveCmdResult, getLastTen } from "./db";
import { executeController, historyController } from "./controllers";

jest.mock("./executor");
jest.mock("./db");

const execMock = executeCmd as any as jest.Mock;
const saveMock = saveCmdResult as any as jest.Mock;
const getLastMock = getLastTen as any as jest.Mock;

describe("controllers", () => {
  let send: jest.Mock;

  beforeEach(() => {
    send = jest.fn();
  });

  describe("executeController", () => {
    test("should execute given command and log result", async () => {
      execMock.mockResolvedValue({ a: 1 });
      saveMock.mockResolvedValue("any");

      await executeController({ query: { cmd: "ls" } } as any, { send } as any);

      expect(execMock).toHaveBeenCalledWith("ls");
      expect(send).toHaveBeenCalledWith(JSON.stringify({ a: 1 }));
      expect(saveMock).toHaveBeenCalledWith({ a: 1 });
    });
  });

  describe("historyController", () => {
    test("should send results from db", async () => {
      getLastMock.mockResolvedValue([1, 2, 3]);

      await historyController({} as any, { send } as any);

      expect(send).toHaveBeenCalledWith(JSON.stringify([1, 2, 3]));
    });
  });
});
