import express from "express";
import cors from "cors";

import { executeController, historyController } from "./controllers";

const app = express();
const port = 3000;

app.use(cors());

app.get("/api/execute", executeController);

app.get("/api/history", historyController);

app.listen(port, () => {
  console.log(`app started on port ${port}`);
});
