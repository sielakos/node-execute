import { exec } from "child_process";
import { executeCmd, ExecuteResponse } from "./executor";

jest.mock("child_process");

const execMock: jest.Mock = exec as any;

describe("executor", () => {
  describe("executeCmd", () => {
    let executeCallback: (error: any, stdout?: string, stderr?: string) => void;

    beforeEach(() => {
      execMock.mockImplementation(
        (
          cmd: string,
          options: any,
          callback: (error: any, stdout?: string, stderr?: string) => void
        ) => {
          executeCallback = callback;
        }
      );
    });

    test("should execute given command", () => {
      executeCmd("ls", 4000);

      expect(execMock.mock.calls[0][0]).toBe("ls");
      expect(execMock.mock.calls[0][1]).toEqual({ timeout: 4000 });
    });

    test("should resolve promise with stdout", () => {
      const promise = executeCmd("ls");

      executeCallback(undefined, "test-1");

      return promise.then(({ cmd, stdout }: ExecuteResponse) => {
        expect(cmd).toBe("ls");
        expect(stdout).toBe("test-1");
      });
    });

    test("should resolve promise with stderr", () => {
      const promise = executeCmd("ls");

      executeCallback(undefined, undefined, "test-err");

      return promise.then(({ cmd, stderr }: ExecuteResponse) => {
        expect(cmd).toBe("ls");
        expect(stderr).toBe("test-err");
      });
    });

    test("should resolve promise with error", () => {
      const promise = executeCmd("ls");

      executeCallback("test-err");

      return promise.then(({ cmd, error }: ExecuteResponse) => {
        expect(cmd).toBe("ls");
        expect(error).toBe("test-err");
      });
    });
  });
});
