import { client, saveCmdResult, getLastTen } from "./db";

describe("db", () => {
  describe("getLastTen", () => {
    test("should get last results", async () => {
      (client as any).toArray.mockReturnValue([1, 2, 3, 4]);

      const result = await getLastTen();

      expect(result).toEqual([1, 2, 3, 4]);
    });

    test("should fail when could not find result", async () => {
      (client as any).limit.mockReturnValue(Promise.reject("reason"));

      let reason;

      try {
        await getLastTen();
      } catch (error) {
        reason = error;
      }

      expect(reason).toBe("reason");
    });
  });
});
