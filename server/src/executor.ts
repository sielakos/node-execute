import { exec } from "child_process";

export interface ExecuteResponse {
  cmd: string;
  error?: string;
  stdout?: string;
  stderr?: string;
}

export function executeCmd(
  cmd: string,
  timeout = 5000
): Promise<ExecuteResponse> {
  return new Promise((resolve) => {
    const process = exec(cmd, { timeout }, (error, stdout, stderr) => {
      if (error) {
        return resolve({ cmd, error: error.toString() });
      }

      resolve({
        cmd,
        stdout,
        stderr,
      });
    });
  });
}
