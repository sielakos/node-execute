import { Request, Response } from "express";

import { executeCmd } from "./executor";
import { saveCmdResult, getLastTen } from "./db";

export async function executeController(req: Request, res: Response) {
  const cmd = req.query.cmd as string;

  const result = await executeCmd(cmd);

  console.log(`log: executed command ${cmd} got result `, result);
  await saveCmdResult(result);
  res.send(JSON.stringify(result));
}

export async function historyController(req: Request, res: Response) {
  const results = await getLastTen();

  res.send(JSON.stringify(results));
}
