import { MongoClient } from "mongodb";

import { ExecuteResponse } from "./executor";

console.log("db url", process.env.MONGO_URL);

export const client = new MongoClient(
  process.env.MONGO_URL ?? "mongodb://root:example@localhost:27017",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

client.connect().catch(console.error);

export async function saveCmdResult(result: ExecuteResponse) {
  const database = client.db("executor");
  const results = database.collection("results");
  const timestamp = new Date();

  await results.insertOne({ ...result, timestamp });
}

export async function getLastTen() {
  const database = client.db("executor");
  const results = database.collection("results");

  const cursor = await results.find().sort({ timestamp: -1 }).limit(10);

  return cursor.toArray();
}
