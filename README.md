# building

`docker-compose build`

# running

`docker-compose up`

# notes

- Tests provided only for server side application
- Application structure was kept simple, because requirements were simple. So according to KISS no need to overcomplicate.
- Whole thing runs in dev mode as production quality builds were not my aim here.
