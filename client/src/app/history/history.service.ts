import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { switchMap, merge } from 'rxjs/operators';

import { environment } from '@/environments/environment';

import { HistoryEntry } from './interfaces';

@Injectable({
  providedIn: 'root',
})
export class HistoryService {
  private refresh$: Subject<unknown> = new Subject();

  constructor(private http: HttpClient) {}

  public getLastTen(): Observable<HistoryEntry[]> {
    return of([1]).pipe(
      merge(this.refresh$),
      switchMap(() =>
        this.http.get<HistoryEntry[]>(`${environment.host}/api/history`)
      )
    );
  }

  public refresh() {
    this.refresh$.next();
  }
}
