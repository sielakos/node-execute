import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { HistoryComponent } from './history/history.component';

@NgModule({
  declarations: [HistoryComponent],
  imports: [CommonModule, HttpClientModule],
  exports: [HistoryComponent],
})
export class HistoryModule {}
