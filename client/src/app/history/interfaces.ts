import { ExecuteResponse } from '@/app/execute/interfaces';

export interface HistoryEntry extends ExecuteResponse {
  timestamp: Date;
}
