import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { HistoryService } from '@/app/history/history.service';
import { HistoryEntry } from '@/app/history/interfaces';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnInit {
  public entries$: Observable<HistoryEntry[]> | undefined;

  constructor(private history: HistoryService) {}

  ngOnInit(): void {
    this.entries$ = this.history.getLastTen();
  }
}
