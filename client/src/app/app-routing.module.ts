import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ExecuteComponent } from './execute/execute/execute.component';

const routes: Routes = [{
  path: '',
  component: ExecuteComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
