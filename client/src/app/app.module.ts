import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExecuteModule } from './execute/execute.module';
import { HistoryModule } from './history/history.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, ExecuteModule, HistoryModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
