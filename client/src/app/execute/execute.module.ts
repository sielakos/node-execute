import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ExecuteComponent } from './execute/execute.component';

@NgModule({
  declarations: [ExecuteComponent],
  imports: [CommonModule, HttpClientModule, FormsModule],
  exports: [ExecuteComponent],
})
export class ExecuteModule {}
