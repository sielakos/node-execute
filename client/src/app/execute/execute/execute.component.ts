import { Component } from '@angular/core';

import { ExecuteService } from '@/app/execute/execute.service';
import { ExecuteResponse } from '@/app/execute/interfaces';

@Component({
  selector: 'app-execute',
  templateUrl: './execute.component.html',
  styleUrls: ['./execute.component.scss'],
})
export class ExecuteComponent {
  public result: ExecuteResponse | undefined;
  public cmd: string = 'ls';

  constructor(private executeService: ExecuteService) {}

  public executeCmd() {
    this.executeService.executeCmd(this.cmd).subscribe(
      (result) => {
        this.result = result;
      },
      (error) =>
        (this.result = {
          cmd: this.cmd,
          error: JSON.stringify(error, undefined, 2),
        })
    );
  }
}
