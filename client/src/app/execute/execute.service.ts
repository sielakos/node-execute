import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '@/environments/environment';
import { HistoryService } from '@/app/history/history.service';

import { ExecuteResponse } from './interfaces';

@Injectable({
  providedIn: 'root',
})
export class ExecuteService {
  constructor(private http: HttpClient, private history: HistoryService) {}

  public executeCmd(cmd: string): Observable<ExecuteResponse> {
    return this.http
      .get<ExecuteResponse>(`${environment.host}/api/execute`, {
        params: { cmd },
      })
      .pipe(tap(() => this.history.refresh()));
  }
}
