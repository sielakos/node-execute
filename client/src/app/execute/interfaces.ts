export interface ExecuteResponse {
  cmd: string;
  error?: string;
  stdout?: string;
  stderr?: string;
}
